#include <rili/service/Console.hpp>
#include <thread>

namespace {
void fps() {
    auto& console = rili::service::Console::instance();
    auto start = std::chrono::high_resolution_clock::now();
    auto now = start;
    auto end = now + std::chrono::seconds(10);
    int fps = 0;
    for (int i = 0; end > now; i++) {
        auto size = console.size();
        console.clear();
        console.moveCursor(size.first / 2, size.second / 2);
        console << fps << ": " << size.first << "x" << size.second << rili::stream::flush;
        console.moveCursor(size.first - 1, size.second - 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(33));
        now = std::chrono::high_resolution_clock::now();
        if (std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count() > 1000) {
            start = now;
            fps = i;
            i = 0;
        }
    }
}

void colors() {
    auto& console = rili::service::Console::instance();
    console.clear();
    for (int color = 0; color < 256; color++) {
        console << color << "\t[";
        console << rili::service::console::bg(rili::color::ANSI256(static_cast<std::uint8_t>(color))) << "  "
                << rili::stream::flush;
        console << rili::service::console::bg(rili::color::ANSI256(static_cast<std::uint8_t>(color)).ansi16()) << "  "
                << rili::stream::flush;
        console << rili::service::console::bg();
        console << "]" << rili::stream::endl;
    }
    int k;
    console >> k;
}
}  // namespace
int main(int, char**) {
    auto& console = rili::service::Console::instance();
    if (!console.outputMode(rili::service::Console::OutputMode::VT100_216Colors)) {
        console.outputMode(rili::service::Console::OutputMode::WINDOWS_API);
    }
    fps();
    colors();
    return 0;
}
