# Rili Console service

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of console/terminal operation service and other command line related tools

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/service/console)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/service/console/badges/master/build.svg)](https://gitlab.com/rilis/rili/service/console/commits/master)
