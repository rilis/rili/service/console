cmake_minimum_required(VERSION 3.0.0)
project(rili-service-console)

cmake_policy(SET CMP0054 NEW)

unset(MSVC)
if (${CMAKE_CXX_COMPILER_ID} STREQUAL "AppleClang" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wconversion -Wmissing-declarations -fno-rtti -fexceptions  -fstack-protector -pedantic -Wall -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdeprecated -Werror -Wextra -Winit-self -Wnon-virtual-dtor -Wno-padded -Wno-shadow -Wno-unused-macros -Wold-style-cast -Wpedantic -Wpointer-arith -Wsign-promo -Wstrict-aliasing -Wuninitialized")
elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL "MSVC")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /bigobj")
else()
  message(FATAL_ERROR "Unsupported compiler:${CMAKE_CXX_COMPILER_ID}")
endif()

option(RILI_SERVICE_CONSOLE_COVERAGE "If enabled rili service console will be compiled with coverage options" OFF)
option(RILI_SERVICE_CONSOLE_TESTS "If enabled rili service console tests will be build" OFF)

if(RILI_SERVICE_CONSOLE_COVERAGE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
endif()

# begin feature detection
include(CheckIncludeFiles)
CHECK_INCLUDE_FILES("unistd.h" RILI_HAVE_UNISTD_H)
CHECK_INCLUDE_FILES("sys/ioctl.h" RILI_HAVE_SYS_IOCTL_H)
CHECK_INCLUDE_FILES("windows.h" RILI_HAVE_WINDOWS_H)


configure_file("${CMAKE_CURRENT_SOURCE_DIR}/Config.hxx.in" "${CMAKE_CURRENT_SOURCE_DIR}/rili/service/ConsoleConfig.hxx" @ONLY)
# end feature detection

set(SOURCES
    rili/service/Console.cpp
    rili/service/Console.hpp
    rili/service/ConsoleConfig.hxx
)

set(TESTS
  tests/main.cpp
)


find_package(rili-stream REQUIRED)
find_package(rili-color REQUIRED)
include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${rili-stream_INCLUDE_DIRS} ${rili-color_INCLUDE_DIRS})

add_library(${PROJECT_NAME} STATIC ${SOURCES})
target_link_libraries(${PROJECT_NAME} rili-stream rili-color)

install(TARGETS ${PROJECT_NAME}
  EXPORT ${PROJECT_NAME}Targets
  ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" COMPONENT staticlib)

install(DIRECTORY rili DESTINATION "${CMAKE_INSTALL_PREFIX}/include" FILES_MATCHING PATTERN "*.hpp")

set(INSTALL_CMAKE_CONFIG_DIR "${CMAKE_INSTALL_PREFIX}/lib/cmake/${PROJECT_NAME}")
configure_file("${CMAKE_CURRENT_SOURCE_DIR}/config.cmake.in" "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake" @ONLY)
install(EXPORT ${PROJECT_NAME}Targets DESTINATION "${INSTALL_CMAKE_CONFIG_DIR}" COMPONENT dev)
install(FILES "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake" DESTINATION "${INSTALL_CMAKE_CONFIG_DIR}" COMPONENT dev)

if(RILI_SERVICE_CONSOLE_TESTS)
  find_package(rili-test REQUIRED)
  include_directories(tests ${rili-test_INCLUDE_DIRS})

  add_executable(${PROJECT_NAME}_tests ${TESTS})
  target_link_libraries(${PROJECT_NAME}_tests rili-test ${PROJECT_NAME})

  add_executable(colorMatcher tests/colorMatcher.cpp)
  target_link_libraries(colorMatcher ${PROJECT_NAME})
endif()

set(CONFIG_FILES
  Config.hxx.in
  .clang-format
  .gitlab-ci.yml
  .hell.json
  .gitignore
  Doxyfile
  LICENSE.md
  README.md
)

add_custom_target(${PROJECT_NAME}-IDE SOURCES ${CONFIG_FILES})
