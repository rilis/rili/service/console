/* @file */
#pragma once
#include <cstdint>
#include <functional>
#include <rili/Color.hpp>
#include <rili/Stream.hpp>
#include <utility>

namespace rili {
namespace service {
class ConsoleBase;
namespace console {
/**
 * @brief Formater is function type used to modify Console propperties in stream way
 */
typedef std::function<ConsoleBase&(ConsoleBase&)> Formater;
}

/**
 * @brief The ConsoleBase class is interface for all Console service implementations
 */
class ConsoleBase : public stream::Duplex {
 public:
    virtual ~ConsoleBase() = default;

 public:
    /**
     * @brief backgroundColor set given color as background if avaliable in seleted mode
     * @param color used as background color
     */
    virtual void backgroundColor(Color const& color) = 0;
    /**
     * @brief foregroundColor set given color as foreground if avaliable in seleted mode
     * @param color used as foreground color
     */
    virtual void foregroundColor(Color const& color) = 0;
    /**
     * @brief defaultBackgroundColor reset background color to default
     */
    virtual void defaultBackgroundColor() = 0;
    /**
     * @brief defaultForegroundColor reset foreground color to default
     */
    virtual void defaultForegroundColor() = 0;
    /**
     * @brief size aquire current size of console viewport
     * @return pair containing number of columnts and lines in viewport
     */
    virtual std::pair<std::uint32_t, std::uint32_t> size() = 0;
    /**
     * @brief moveCursor set move cursor to given position
     * @param column
     * @param line
     */
    virtual void moveCursor(std::uint32_t column, std::uint32_t line) = 0;
    /**
     * @brief clear reset all output painting attributes, clear viewport and move cursor to top left corner
     */
    virtual void clear() = 0;

    /**
     * @brief The OutputMode enum used to select output mode. VT100 based will not work on windows, WINDOWS_API
     * may work only on windows. NONE is default mode in which all output formationg is disabled
     */
    enum class OutputMode { VT100_16Colors, VT100_216Colors, VT100_RGBColors, WINDOWS_API, NONE };

    /**
     * @brief outputMode try change mode to given one
     * @param mode used to select mode
     * @param force if set to true sets given mode even if might not be fully supported
     * @return true if mode succesfully changed, otherwise false
     */
    virtual bool outputMode(OutputMode mode, bool force = false) = 0;

    /// @cond INTERNAL
    inline ConsoleBase& operator<<(console::Formater const& fn) { return fn(*this); }
    template <typename T>
    ConsoleBase& operator<<(T const& t) {
        stream::Writable& self = *this;
        self << t;
        return *this;
    }
    template <typename T>
    rili::stream::OperationResult operator>>(T& t) {
        stream::Readable& self = *this;
        return self >> t;
    }
    /// @endcond INTERNAL

 private:
    ConsoleBase(ConsoleBase const& other) = delete;
    ConsoleBase& operator=(ConsoleBase const& other) = delete;

 protected:
    ConsoleBase() = default;
};

/**
 * @brief The Console class is simple implementation of Console service
 */
class Console final : public ConsoleBase {
 public:
    /**
     * @brief instance used to aquire console service
     * @return instance of Console service
     */
    static ConsoleBase& instance();
    virtual ~Console() = default;

 public:
    void backgroundColor(Color const& color) override;
    void foregroundColor(Color const& color) override;
    void defaultBackgroundColor() override;
    void defaultForegroundColor() override;
    std::pair<std::uint32_t, std::uint32_t> size() override;
    void moveCursor(std::uint32_t column, std::uint32_t line) override;
    void clear() override;

    /**
     * @brief outputMode try change mode to given one
     * @param mode used to select mode
     * @param force if set to true sets given mode even if might not be fully supported
     * @return true if mode succesfully changed, otherwise false
     */
    bool outputMode(OutputMode mode, bool force = false) override;

 public:
    /// @cond INTERNAL
    void consume(std::size_t size) override;
    stream::view::Base const& readableView() const override;
    stream::view::Base const& writableView() const override;
    std::exception_ptr pull(std::size_t count) override;
    void write(char const* data, std::size_t size) override;
    std::exception_ptr flush() override;
    /// @endcond INTERNAL

 private:
    Console();
    Console(Console const& other) = delete;
    Console& operator=(Console const& other) = delete;

 private:
    stream::Writable& m_writable;
    stream::Readable& m_readable;
    OutputMode m_mode;
};

namespace console {
/**
 * @brief bg is Formater generator used to set background in Console with given color in stream way
 * @param color to be used as background color
 * @return Formater instance
 */
template <typename ColorType>
Formater bg(ColorType color) {
    return [color](ConsoleBase& console) -> ConsoleBase& {
        console.backgroundColor(color);
        return console;
    };
}

/**
 * @brief fg is Formater generator used to set foreground in Console with given color in stream way
 * @param color to be used as foreground color
 * @return Formater instance
 */
template <typename ColorType>
Formater fg(ColorType color) {
    return [color](ConsoleBase& console) -> ConsoleBase& {
        console.foregroundColor(color);
        return console;
    };
}

/**
 * @brief bg is Formater generator used to reset background color in Console
 * @return Formater instance
 */
Formater bg();

/**
 * @brief bg is Formater generator used to reset foreground color in Console
 * @return Formater instance
 */
Formater fg();

/**
 * @brief clear is Formater generator used to clear Console
 * @return Formater instance
 */
Formater clear();

/**
 * @brief move is Formater generator used to move cursor in Console
 * @param column
 * @param line
 * @return Formater instance
 */
Formater move(std::uint32_t column, std::uint32_t line);
}  // namespace console
}  // namespace service
}  // namespace rili
