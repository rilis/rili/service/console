#include <rili/service/ConsoleConfig.hxx>

#ifdef RILI_HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif
#ifdef RILI_HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef RILI_HAVE_WINDOWS_H
#include <windows.h>
#undef RGB
#undef CMYK
#endif
#include <cmath>
#include <rili/service/Console.hpp>
#include <utility>

namespace rili {
namespace service {
namespace {
#if defined(RILI_HAVE_WINDOWS_H)
static WORD defaultBackgroundAttributes = 0;
static WORD defaultForegroundAttributes = 0;
static HANDLE consoleHandle = nullptr;
inline WORD ansi16ToWindowsBgColorAttribute(std::uint8_t v) {
    switch (v) {
        case 0:
            return 0;
        case 1:
            return BACKGROUND_RED;
        case 2:
            return BACKGROUND_GREEN;
        case 3:
            return BACKGROUND_RED | BACKGROUND_GREEN;
        case 4:
            return BACKGROUND_BLUE;
        case 5:
            return BACKGROUND_RED | BACKGROUND_BLUE;
        case 6:
            return BACKGROUND_GREEN | BACKGROUND_BLUE;
        case 7:
            return BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;
        case 8:
            return BACKGROUND_INTENSITY;
        case 9:
            return BACKGROUND_INTENSITY | BACKGROUND_RED;
        case 10:
            return BACKGROUND_INTENSITY | BACKGROUND_GREEN;
        case 11:
            return BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN;
        case 12:
            return BACKGROUND_INTENSITY | BACKGROUND_BLUE;
        case 13:
            return BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_BLUE;
        case 14:
            return BACKGROUND_INTENSITY | BACKGROUND_GREEN | BACKGROUND_BLUE;
        case 15:
            return BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;
        default:
            return 0;
    }
}

inline WORD ansi16ToWindowsFgColorAttribute(std::uint8_t v) {
    switch (v) {
        case 0:
            return 0;
        case 1:
            return FOREGROUND_RED;
        case 2:
            return FOREGROUND_GREEN;
        case 3:
            return FOREGROUND_RED | FOREGROUND_GREEN;
        case 4:
            return FOREGROUND_BLUE;
        case 5:
            return FOREGROUND_RED | FOREGROUND_BLUE;
        case 6:
            return FOREGROUND_GREEN | FOREGROUND_BLUE;
        case 7:
            return FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
        case 8:
            return FOREGROUND_INTENSITY;
        case 9:
            return FOREGROUND_INTENSITY | FOREGROUND_RED;
        case 10:
            return FOREGROUND_INTENSITY | FOREGROUND_GREEN;
        case 11:
            return FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN;
        case 12:
            return FOREGROUND_INTENSITY | FOREGROUND_BLUE;
        case 13:
            return FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE;
        case 14:
            return FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE;
        case 15:
            return FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
        default:
            return 0;
    }
}
#endif
}  // namespace
void Console::backgroundColor(Color const &color) {
    m_writable << stream::flush;
#if defined(RILI_HAVE_WINDOWS_H)
    if (m_mode == OutputMode::WINDOWS_API) {
        if (consoleHandle == nullptr) {
            return;
        }
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        if (!::GetConsoleScreenBufferInfo(consoleHandle, &csbi)) {
            return;
        }
        auto attributes = csbi.wAttributes;
        attributes &= ~(BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE);
        attributes |= ansi16ToWindowsBgColorAttribute(color.ansi16().value);
        ::SetConsoleTextAttribute(consoleHandle, attributes);
    }
#else
    if (m_mode == OutputMode::VT100_RGBColors) {
        color::RGB rgb = color.rgb();
        m_writable << "\x1b[48;2;" << static_cast<std::uint16_t>(rgb.red) << ";"
                   << static_cast<std::uint16_t>(rgb.green) << ";" << static_cast<std::uint16_t>(rgb.blue) << "m"
                   << stream::flush;
    } else if (m_mode == OutputMode::VT100_216Colors) {
        color::ANSI256 ansi256 = color.ansi256();
        m_writable << "\x1b[48;5;" << static_cast<std::uint16_t>(ansi256.value) << "m" << stream::flush;
    } else if (m_mode == OutputMode::VT100_16Colors) {
        std::uint16_t ansi16 = static_cast<std::uint16_t>(color.ansi16().value);
        if (ansi16 < 8) {
            ansi16 = static_cast<std::uint16_t>(ansi16 + 40);
        } else {
            ansi16 = static_cast<std::uint16_t>(ansi16 + 100 - 8);
        }
        m_writable << "\x1b[" << ansi16 << "m" << stream::flush;
    }
#endif
}

void Console::foregroundColor(Color const &color) {
    m_writable << stream::flush;
#if defined(RILI_HAVE_WINDOWS_H)
    if (m_mode == OutputMode::WINDOWS_API) {
        if (consoleHandle == nullptr) {
            return;
        }
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        if (!::GetConsoleScreenBufferInfo(consoleHandle, &csbi)) {
            return;
        }
        auto attributes = csbi.wAttributes;
        attributes &= ~(FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
        attributes |= ansi16ToWindowsFgColorAttribute(color.ansi16().value);
        ::SetConsoleTextAttribute(consoleHandle, attributes);
    }
#else
    if (m_mode == OutputMode::VT100_RGBColors) {
        color::RGB rgb = color.rgb();
        m_writable << "\x1b[38;2;" << static_cast<std::uint16_t>(rgb.red) << ";"
                   << static_cast<std::uint16_t>(rgb.green) << ";" << static_cast<std::uint16_t>(rgb.blue) << "m"
                   << stream::flush;
    } else if (m_mode == OutputMode::VT100_216Colors) {
        color::ANSI256 ansi256 = color.ansi256();
        m_writable << "\x1b[38;5;" << static_cast<std::uint16_t>(ansi256.value) << "m" << stream::flush;
    } else if (m_mode == OutputMode::VT100_16Colors) {
        std::uint16_t ansi16 = static_cast<std::uint16_t>(color.ansi16().value);
        if (ansi16 < 8) {
            ansi16 = static_cast<std::uint16_t>(ansi16 + 30);
        } else {
            ansi16 = static_cast<std::uint16_t>(ansi16 + 90 - 8);
        }
        m_writable << "\x1b[" << ansi16 << "m" << stream::flush;
    }
#endif
}

void Console::defaultBackgroundColor() {
    m_writable << stream::flush;
#if defined(RILI_HAVE_WINDOWS_H)
    if (m_mode == OutputMode::WINDOWS_API) {
        if (consoleHandle == nullptr) {
            return;
        }
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        if (!::GetConsoleScreenBufferInfo(consoleHandle, &csbi)) {
            return;
        }
        auto attributes = csbi.wAttributes;
        attributes &= ~(BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE);
        attributes |= defaultBackgroundAttributes;
        ::SetConsoleTextAttribute(consoleHandle, attributes);
    }
#else
    if (m_mode == OutputMode::VT100_16Colors || m_mode == OutputMode::VT100_216Colors ||
        m_mode == OutputMode::VT100_RGBColors) {
        m_writable << "\x1b[49m" << stream::flush;
    }
#endif
}

void Console::defaultForegroundColor() {
    m_writable << stream::flush;
#if defined(RILI_HAVE_WINDOWS_H)
    if (m_mode == OutputMode::WINDOWS_API) {
        if (consoleHandle == nullptr) {
            return;
        }
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        if (!::GetConsoleScreenBufferInfo(consoleHandle, &csbi)) {
            return;
        }
        auto attributes = csbi.wAttributes;
        attributes &= ~(FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
        attributes |= defaultForegroundAttributes;
        ::SetConsoleTextAttribute(consoleHandle, attributes);
    }
#else
    if (m_mode == OutputMode::VT100_16Colors || m_mode == OutputMode::VT100_216Colors ||
        m_mode == OutputMode::VT100_RGBColors) {
        m_writable << "\x1b[39m" << stream::flush;
    }
#endif
}

std::pair<std::uint32_t, std::uint32_t> Console::size() {
#if defined(RILI_HAVE_SYS_IOCTL_H) && defined(RILI_HAVE_UNISTD_H)
    {
        struct winsize size;
        if (-1 != ::ioctl(STDOUT_FILENO, TIOCGWINSZ, &size)) {
            return {size.ws_col, size.ws_row};
        }
    }
#elif defined(RILI_HAVE_WINDOWS_H)
    {
        if (consoleHandle != nullptr) {
            CONSOLE_SCREEN_BUFFER_INFO csbi;
            int ret;
            ret = ::GetConsoleScreenBufferInfo(consoleHandle, &csbi);
            if (ret) {
                return {csbi.srWindow.Right - csbi.srWindow.Left, csbi.srWindow.Bottom - csbi.srWindow.Top};
            }
        }
    }
#endif
    return {0, 0};
}

void Console::moveCursor(std::uint32_t column, std::uint32_t line) {
    m_writable << stream::flush;
#if defined(RILI_HAVE_WINDOWS_H)
    if (m_mode == OutputMode::WINDOWS_API) {
        if (consoleHandle) {
            COORD coordScreen;
            coordScreen.X = column;
            coordScreen.Y = line;
            ::SetConsoleCursorPosition(consoleHandle, coordScreen);
        }
    }
#else
    if (m_mode == OutputMode::VT100_16Colors || m_mode == OutputMode::VT100_216Colors ||
        m_mode == OutputMode::VT100_RGBColors) {
        m_writable << "\x1b[" << line + 1 << ";" << column + 1 << "f" << stream::flush;
    }
#endif
}

void Console::clear() {
    m_writable << stream::flush;
#if defined(RILI_HAVE_WINDOWS_H)
    if (m_mode == OutputMode::WINDOWS_API) {
        if (consoleHandle == nullptr) {
            return;
        }
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        DWORD cCharsWritten;
        COORD coordScreen = {0, 0};
        if (!GetConsoleScreenBufferInfo(consoleHandle, &csbi)) {
            return;
        }
        DWORD dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
        if (!::FillConsoleOutputCharacter(consoleHandle, (TCHAR)' ', dwConSize, coordScreen, &cCharsWritten)) {
            return;
        }
        auto attributes = csbi.wAttributes;
        attributes &= ~(FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE |
                        BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE);
        if (!::FillConsoleOutputAttribute(consoleHandle,
                                          attributes | defaultBackgroundAttributes | defaultForegroundAttributes,
                                          dwConSize, coordScreen, &cCharsWritten)) {
            return;
        }
        ::SetConsoleCursorPosition(consoleHandle, coordScreen);
    }
#else
    if (m_mode == OutputMode::VT100_16Colors || m_mode == OutputMode::VT100_216Colors ||
        m_mode == OutputMode::VT100_RGBColors) {
        m_writable << "\x1b[2J\x1b[1;1f\x1b[0m" << stream::flush;
    }
#endif
}

bool Console::outputMode(ConsoleBase::OutputMode mode, bool force) {
    switch (mode) {
        case OutputMode::NONE: {
            m_mode = mode;
            return true;
        }
#if defined(RILI_HAVE_WINDOWS_H)
        case OutputMode::WINDOWS_API: {
            consoleHandle = ::GetStdHandle(STD_OUTPUT_HANDLE);
            if (consoleHandle != nullptr || force) {
                m_mode = mode;
                return true;
            } else {
                return false;
            }
        }
#else
        case OutputMode::VT100_16Colors:
        case OutputMode::VT100_216Colors:
        case OutputMode::VT100_RGBColors: {
            if (::isatty(STDOUT_FILENO) == 1 || force) {
                m_mode = mode;
                return true;
            } else {
                return false;
            }
        }
#endif
        default: { return false; }
    }
}

ConsoleBase &Console::instance() {
    static Console instance;
    return instance;
}

void Console::consume(std::size_t size) { m_readable.consume(size); }
const stream::view::Base &Console::readableView() const { return m_readable.readableView(); }
const stream::view::Base &Console::writableView() const { return m_writable.writableView(); }
std::exception_ptr Console::pull(std::size_t count) { return m_readable.pull(count); }
void Console::write(const char *data, std::size_t size) { m_writable.write(data, size); }
std::exception_ptr Console::flush() { return m_writable.flush(); }
Console::Console() : ConsoleBase(), m_writable(stream::cout()), m_readable(stream::cin()), m_mode(OutputMode::NONE) {
#if defined(RILI_HAVE_WINDOWS_H)
    auto lConsole = ::GetStdHandle(STD_OUTPUT_HANDLE);
    if (lConsole == nullptr) {
        return;
    }
    CONSOLE_SCREEN_BUFFER_INFO csbi;
    if (!::GetConsoleScreenBufferInfo(lConsole, &csbi)) {
        return;
    }
    defaultForegroundAttributes =
        csbi.wAttributes & (FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    defaultBackgroundAttributes =
        csbi.wAttributes & (BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE);
#endif
}

namespace console {

Formater bg() {
    return [](ConsoleBase &console) -> ConsoleBase & {
        console.defaultBackgroundColor();
        return console;
    };
}

Formater fg() {
    return [](ConsoleBase &console) -> ConsoleBase & {
        console.defaultForegroundColor();
        return console;
    };
}

Formater clear() {
    return [](ConsoleBase &console) -> ConsoleBase & {
        console.clear();
        return console;
    };
}

Formater move(uint32_t column, uint32_t line) {
    return [column, line](ConsoleBase &console) -> ConsoleBase & {
        console.moveCursor(column, line);
        return console;
    };
}

}  // namespace console

}  // namespace service
}  // namespace rili
